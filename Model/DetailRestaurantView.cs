﻿using FHamel_ELMarion_tp3.Models.ClasseBD;
using FHamel_ELMarion_tp3.Models.ClasseBD.ClassesRelations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace FHamel_ELMarion_tp3.Models
{
    public class DetailRestaurantView
    {

        public int RestaurantID { get; set; }

        [Required(ErrorMessage = "Le nom est obligatoire.")]
        public string NomRestaurant { get; set; }

        [Required(ErrorMessage = "La photo est obligatoire.")]
        [DataType(DataType.Upload)]
        public string Photo { get; set; }

        [Required(ErrorMessage = "La cote est obligatoire.")]
        public double CotesMembres { get; set; }

        [Required(ErrorMessage = "La description est obligatoire.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Le type de cuisine est obligatoire.")]
        public virtual TypeCuisine TypeCuisineRestaurant { get; set; }

        [Required(ErrorMessage = "Le prix approximatif pour deux est obligatoire.")]
        public double PrixApproxPour2 { get; set; }

        [DataType(DataType.Url)]
        public string SiteWeb { get; set; }

        [Range(1900, 2017)]
        public int AnneeOuverture { get; set; }


        public virtual ICollection<Commentaire> LstCommentaires { get; set; }

        public virtual ICollection<RelUtilResto> LstRelUtilResto { get; set; }

        public int NbrVote { get; set; }

        
        [Required(ErrorMessage = "La note est obligatoire")]
        public double Classement { get; set; }

        [MaxLength(255,ErrorMessage = "Votre commentaire ne devrait pas dépasser 255 caractères")]
        public string Contenu { get; set; }

        [Required(ErrorMessage = "La note  est obligatoire.")]
        [Range(1, 5, ErrorMessage = "La note doit se situer entre 1 et 5")]
        public int Note { get; set; }

        public virtual ApplicationUser UnUtilisateur { get; set; }

        public bool Aime { get; set; }
        public DetailRestaurantView()
        {
            LstCommentaires = new List<Commentaire>();
            LstRelUtilResto = new List<RelUtilResto>();
        }
    }
}
