﻿using FHamel_ELMarion_tp3.Models.ClasseBD.ClassesRelations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FHamel_ELMarion_tp3.Models.ClasseBD
{
    public class RechercheRestaurant
    {
        public List<Restaurant> lstRestaurant { get; set; }

        public ApplicationUser Utilisateur { get; set; }


    }
}
