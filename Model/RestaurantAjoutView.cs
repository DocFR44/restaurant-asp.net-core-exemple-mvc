﻿using FHamel_ELMarion_tp3.Data;
using FHamel_ELMarion_tp3.Models.ClasseBD;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace FHamel_ELMarion_tp3.Models
{
    public class RestaurantAjoutView
    {
        [Required(ErrorMessage = "Le nom est obligatoire.")]
        [MaxLength(255, ErrorMessage = "Le titre doit avoir un maximum de 255 caractères.")]
        public string NomRestaurant { get; set; }

        [Required(ErrorMessage = "La photo est obligatoire.")]
        [DataType(DataType.Upload)]
        public IFormFile Photo { get; set; }

        [Required(ErrorMessage = "La cote est obligatoire.")]
        public double CotesMembres { get; set; }

        [Required(ErrorMessage = "La description est obligatoire.")]
        [MaxLength(1000, ErrorMessage = "La description doit avoir un maximum de 1000 caractères.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Le type de cuisine est obligatoire.")]
        public int TypeCuisineRestaurant { get; set; }

        [Required(ErrorMessage = "Le prix approximatif pour deux est obligatoire.")]
        public double PrixApproxPour2 { get; set; }

        [DataType(DataType.Url)]
        public string SiteWeb { get; set; }

        [Range(1900, 2017)]
        public int AnneeOuverture { get; set; }

        public List<TypeCuisine> lstTypeCuisne {get; set;}


    }
}
