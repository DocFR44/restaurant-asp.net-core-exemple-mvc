using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FHamel_ELMarion_tp3.Data;
using FHamel_ELMarion_tp3.Models.ClasseBD;
using FHamel_ELMarion_tp3.Models;
using System.IO;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using FHamel_ELMarion_tp3.Models.ClasseBD.ClassesRelations;

namespace FHamel_ELMarion_tp3.Controllers
{
    public class RestaurantsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;


        private IHostingEnvironment _environnement;

        public RestaurantsController(IHostingEnvironment environnement, ApplicationDbContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _environnement = environnement;
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;

        }



        // GET: Restaurants
        public async Task<IActionResult> Index()
        {
            return View(await _context.Restaurants.ToListAsync());
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResultatRecherche(string SearchString)
        {
            var listeRestos = _context.Restaurants.Include(i => i.TypeCuisineRestaurant).ToList();
            
            List<Restaurant> RestaurantRecherche = new List<Restaurant>();
            
            string laDescriptionRestaurant;
            foreach (var item in listeRestos)
            {
                if (((item.NomRestaurant.ToLower()).Contains(SearchString.ToLower())) || ((item.TypeCuisineRestaurant.LeTypeCuisine.ToLower()).Contains(SearchString.ToLower())))
                {
                    RestaurantRecherche.Add(item);
                    if (item.Description.Length > 100)
                    {
                        laDescriptionRestaurant = item.Description.Substring(0, 96);
                        item.Description = laDescriptionRestaurant + "...";
                    }
                }
            }
            RestaurantRecherche.Sort((x, y) => y.AnneeOuverture.CompareTo(x.AnneeOuverture));
            return View(RestaurantRecherche);
        }


        // GET: Restaurants/Details/5
        [AllowAnonymous]
        public async Task<IActionResult> DetailRestaurant(int? id, int idR)
        {
            
            var lstresto = _context.Restaurants.OrderByDescending(m => m.CotesMembres).ToList();
   
            int compteur = 0;
            
            bool btermine = false;
            while (btermine)
            {
                if (lstresto[compteur].RestaurantID == id)
                {
                    btermine = false;
                }
            }

            var lstcommentaire = from p in _context.Commentaires where p.UnRestaurant.RestaurantID == id select p;

            if (id == null)
            {
                return NotFound();
            }
            var r = new DetailRestaurantView();
            var restaurant = await _context.Restaurants.Include(m=> m.TypeCuisineRestaurant).SingleOrDefaultAsync(m => m.RestaurantID == id);
            restaurant.LstCommentaires = _context.Commentaires.Include(m => m.UnUtilisateur).Where(m => m.UnRestaurant.RestaurantID == id).ToList();
            
            r.RestaurantID = restaurant.RestaurantID;
            r.Classement = compteur + 1;
            r.NbrVote = lstcommentaire.Count();
            r.CotesMembres = restaurant.CotesMembres;
            r.Description = restaurant.Description;
            r.TypeCuisineRestaurant = restaurant.TypeCuisineRestaurant;
            r.Photo = restaurant.Photo;
            r.PrixApproxPour2 = restaurant.PrixApproxPour2;
            r.SiteWeb = restaurant.SiteWeb;
            r.NomRestaurant = restaurant.NomRestaurant;
            r.AnneeOuverture = restaurant.AnneeOuverture;
            r.LstCommentaires = restaurant.LstCommentaires;
            var CurrentUser = await GetCurrentUserAsync();
            var utilisateur = _context.ApplicationUsers.Include(m => m.LstRelUtilResto).Single(m=>m.Id == CurrentUser.Id);
            r.Aime = false;
            foreach (var item in utilisateur.LstRelUtilResto)
            {
                if (item.RestaurantID == restaurant.RestaurantID)
                {
                    r.Aime = true;
                }
            }
            r.AnneeOuverture = restaurant.AnneeOuverture;
            if (restaurant == null)
            {
                return NotFound();
            }

            return View(r);
        }



        [Authorize]
        [HttpGet]
        public async Task<IActionResult> AimeResto(int idR)
        {
            ApplicationUser utilisateur = await GetCurrentUserAsync();

            RelUtilResto relaton = new RelUtilResto();
            relaton.ApplicationUser = utilisateur;
            relaton.Id = utilisateur.Id;
            Restaurant leRestaurant = _context.Restaurants.Single(m => m.RestaurantID == idR);
            relaton.Restaurant = leRestaurant;
            relaton.RestaurantID = leRestaurant.RestaurantID;
            leRestaurant.LstRelUtilResto.Add(relaton);
            utilisateur.LstRelUtilResto.Add(relaton);
            _context.Add(relaton);

            _context.SaveChanges();



            return RedirectToAction("DetailRestaurant/" + idR);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DetailRestaurant(DetailRestaurantView model, int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            var restaurant = await _context.Restaurants.Include(i => i.LstCommentaires).SingleOrDefaultAsync(m => m.RestaurantID == id);
            ApplicationUser utilisateur = await GetCurrentUserAsync();
            var leCommentaire = _context.Commentaires.Include(i => i.UnUtilisateur).SingleOrDefault(m => (m.UnUtilisateur.Id == utilisateur.Id) && (m.UnRestaurant.RestaurantID == restaurant.RestaurantID));
            //_context.Commentaires.Single(m => m.UnUtilisateur.Id == utilisateur.Id);
            if(model.Contenu == null)
            {
                model.Contenu = "";
            }
            if (model.Note > 0 && model.Note < 6 && model.Contenu.Length < 256)
            {
                if (leCommentaire != null)
                {
                    if (restaurant.LstCommentaires.Count() == 1)
                    {
                        restaurant.CotesMembres = model.Note;
                    }
                    else
                    {
                        restaurant.CotesMembres = ((restaurant.CotesMembres * (restaurant.LstCommentaires.Count())) - leCommentaire.Note) / (restaurant.LstCommentaires.Count() - 1);
                        restaurant.CotesMembres = ((restaurant.CotesMembres * (restaurant.LstCommentaires.Count() - 1)) + model.Note) / (restaurant.LstCommentaires.Count());
                    }
                    foreach (var item in restaurant.LstCommentaires)
                    {
                        if (item.CommentaireID == leCommentaire.CommentaireID)
                        {
                            item.Contenu = model.Contenu;
                            item.Note = model.Note;
                        }
                    }

                }
                else
                {
                    Commentaire com = new Commentaire();
                    com.Contenu = model.Contenu;
                    com.Note = model.Note;
                    com.UnUtilisateur = utilisateur;
                    com.UnRestaurant = restaurant;
                    restaurant.LstCommentaires.Add(com);

                    if (restaurant.CotesMembres == 0)
                    {
                        restaurant.CotesMembres = model.Note;
                    }
                    else
                    {
                        restaurant.CotesMembres = ((restaurant.CotesMembres * (restaurant.LstCommentaires.Count() - 1)) + model.Note) / (restaurant.LstCommentaires.Count());
                    }

                }
                _context.Update(restaurant);

                _context.SaveChanges();
                if (restaurant == null)
                {
                    return NotFound();
                }
            }
            return RedirectToAction("DetailRestaurant/" + id);
            

        }

        // GET: Restaurants/Create
        [Authorize]
        public IActionResult RestaurantAjout()
        {
            var m = new RestaurantAjoutView();
            var lstTypeCuisine = from p in _context.TypesCuisine select p;
            m.lstTypeCuisne = lstTypeCuisine.ToList();
            return View(m);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> RestaurantAjout(RestaurantAjoutView model, IFormFile Photo)
        {
            if (ModelState.IsValid)
            {
                
                Restaurant unRestaurant = new Restaurant();
                if (Photo != null && Photo.Length > 0)
                {

                    var uploads = Path.Combine(_environnement.WebRootPath, "images");
                    var filePath = Path.Combine(uploads, Photo.FileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {

                        await Photo.CopyToAsync(fileStream);
                    }

                    
                }
                unRestaurant.Photo = Photo.FileName;
                unRestaurant.AnneeOuverture = model.AnneeOuverture;
                unRestaurant.CotesMembres = 0;
                unRestaurant.Description = model.Description;
                unRestaurant.NomRestaurant = model.NomRestaurant;
                unRestaurant.PrixApproxPour2 = model.PrixApproxPour2;
                unRestaurant.SiteWeb = model.SiteWeb;
                unRestaurant.TypeCuisineRestaurant = _context.TypesCuisine.SingleOrDefault(m => m.TypeCuisineID == model.TypeCuisineRestaurant);
                _context.Add(unRestaurant);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [Authorize]
        // GET: Restaurants/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {


                if (id == null)
            {
                return NotFound();
            }
            ApplicationUser utilisateur = await GetCurrentUserAsync();
            if (utilisateur.EstAdmin)
            {

                var restaurant = await _context.Restaurants.SingleOrDefaultAsync(m => m.RestaurantID == id);
                if (restaurant == null)
                {
                    return NotFound();
                }
                return View(restaurant);
            }

                return RedirectToAction("index");

        }

        // POST: Restaurants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("RestaurantID,AnneeOuverture,CotesMembres,Description,NomRestaurant,Photo,PrixApproxPour2,SiteWeb")] Restaurant restaurant)
        {

            if (id != restaurant.RestaurantID)
            {
                return NotFound();
            }
            ApplicationUser utilisateur = await GetCurrentUserAsync();
            if (utilisateur.EstAdmin)
            {

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(restaurant);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!RestaurantExists(restaurant.RestaurantID))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction("Index");
                }
                return View(restaurant);
            }
                return RedirectToAction("index");

        }

        // GET: Restaurants/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ApplicationUser utilisateur = await GetCurrentUserAsync();
            if (utilisateur.EstAdmin)
            {

                var restaurant = await _context.Restaurants.SingleOrDefaultAsync(m => m.RestaurantID == id);
                if (restaurant == null)
                {
                    return NotFound();
                }

                return View(restaurant);
            }
                return RedirectToAction("index");
        }

        // POST: Restaurants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            ApplicationUser utilisateur = await GetCurrentUserAsync();
            if (utilisateur.EstAdmin)
            {

                var restaurant = await _context.Restaurants.SingleOrDefaultAsync(m => m.RestaurantID == id);
                _context.Restaurants.Remove(restaurant);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }

        private bool RestaurantExists(int id)
        {
            return _context.Restaurants.Any(e => e.RestaurantID == id);
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }
    }
}
